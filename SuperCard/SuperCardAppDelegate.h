//
//  SuperCardAppDelegate.h
//  SuperCard
//
//  Created by Apple on 1/08/13.
//  Copyright (c) 2013 The Gingerbread Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SuperCardAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
