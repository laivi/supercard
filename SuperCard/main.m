//
//  main.m
//  SuperCard
//
//  Created by Apple on 1/08/13.
//  Copyright (c) 2013 The Gingerbread Man. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SuperCardAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SuperCardAppDelegate class]));
    }
}
