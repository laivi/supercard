//
//  PlayingCardView.m
//  SuperCard
//
//  Created by Apple on 1/08/13.
//  Copyright (c) 2013 The Gingerbread Man. All rights reserved.
//

#import "PlayingCardView.h"

@implementation PlayingCardView
#define CORNER_RADIUS_SIZE 12.0

- (void)drawRect:(CGRect)rect
{
    // Drawing code
    UIBezierPath *roundedRect = [UIBezierPath bezierPathWithRoundedRect:self.bounds cornerRadius:CORNER_RADIUS_SIZE];
    
    [roundedRect addClip];
    
    [[UIColor whiteColor] setFill];
    UIRectFill(self.bounds);
    
    [[UIColor blackColor] setStroke];
    [roundedRect stroke];
    
    [self drawCorners];
}

- (NSString *)rankAsString
{
    return @[@"?",@"A",@"2",@"3",@"4",@"5",@"6",@"7",@"8",@"9",@"10",@"J",@"Q",@"K"][self.rank];
}

#define FONT_SIZE_WIDTH_PERCENTAGE 0.20
#define CORNER_WHITE_SPACING 2.0
- (void)drawCorners
{
    NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.alignment = NSTextAlignmentCenter;

    UIFont *cornerFont = [UIFont systemFontOfSize:self.bounds.size.width * FONT_SIZE_WIDTH_PERCENTAGE];
    
    NSAttributedString *cornerText = [[NSAttributedString alloc] initWithString:[NSString stringWithFormat:@"%@\n%@", [self rankAsString], self.suit] attributes:@{ NSParagraphStyleAttributeName : paragraphStyle, NSFontAttributeName : cornerFont}];

    CGRect textBounds;
    textBounds.origin = CGPointMake(CORNER_WHITE_SPACING, CORNER_WHITE_SPACING);
    textBounds.size = [cornerText size];
    [cornerText drawInRect:textBounds];
    
    //Translate to bottom right corner & rotate 180 degrees
    [self pushContextAndDrawUpsideDown];
    [cornerText drawInRect:textBounds];
    [self popContext];
}

- (void)pushContextAndDrawUpsideDown
{
    //UIGraphicsPushContext?
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    CGContextTranslateCTM(context, self.bounds.size.width
                          , self.bounds.size.height);
    CGContextRotateCTM(context, M_PI);
}

- (void)popContext {
    //UIGraphicsPopContext?
    CGContextRestoreGState(UIGraphicsGetCurrentContext());
}

- (void)setSuit:(NSString *)suit
{
    _suit = suit;
    [self setNeedsDisplay];
}

- (void)setRank:(NSUInteger)rank
{
    _rank = rank;
   [self setNeedsDisplay];
}

- (void)setFaceUp:(BOOL)faceUp
{
    _faceUp = faceUp;
    [self setNeedsDisplay];
}

#pragma mark - Initialization

- (void)setup
{
    // do initialization here
}

- (void)awakeFromNib
{
    [self setup];
}

- (id)initWithFrame:(CGRect)aRect
{
    self = [super initWithFrame:aRect];
    [self setup];
    return self;
}

@end
