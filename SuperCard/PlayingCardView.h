//
//  PlayingCardView.h
//  SuperCard
//
//  Created by Apple on 1/08/13.
//  Copyright (c) 2013 The Gingerbread Man. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayingCardView : UIView
@property (nonatomic) NSUInteger rank;
@property (strong, nonatomic) NSString *suit;

@property (nonatomic) BOOL faceUp;
@end
